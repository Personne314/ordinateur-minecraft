# Ordinateur Minecraft

## Introduction
Ce dépot gitlab contient un projet personnel à but purement éducatif.
Je l'ai réalisé dans le but de mieux comprendre le fonctionnement du hardware d'un ordinateur.
Celui-ci se décompose en 2 parties :
- Une [map Minecraft](#description-de-la-map) qui contient l'ordinateur
- Le code source d'un [compilateur](#description-de-lassembleur) (d'un assembleur en fait) écrit en C++

## Description de la map
WIP
La map étant encore en construction, celle-ci ne sera pas mise en ligne pour le moment. Une première version sera rajoutée au dépot une fois que j'aurais implémenté les principales instructions du CPU, afin de proposer une machine qui puisse exécuter quelques instructions minimales.

L'ordinateur est composé de :
- Un [CPU](#cpu)
- La [RAM](#ram)
- Un [afficheur](#afficheur)

### CPU
Il s'agit d'un CPU 16bits. Plus précisément, les instructions, les registres et le bus d'adresse sont sur 16bits, et le bus de données est sur 8bit. Celui-ci propose des instructions basiques premettant des opérations logiques, d'addition, de soustraction, de saut et d'interraction avec la mémoire. 

Les registres processeurs sont au nombre de 8 :
- AX, BX, CX, DX (16bits) : Registres généraux, chacun décomposé en 2 registres de 8bits (AH et AL, BH...)
- IR (16bits) : Registre qui contient la prochaine instruction à exécuter. 
- PC (8bits) : Compteur de programme, contient l'adresse mémoire de l'instruction en cours d'exécution.
- SR (8bits) : Registre du Stack, contient un pointeur vers le sommet de la pile.
- FR (8bits) : Registre des Flags, modifié par l'instruction de comparaison, permet de faire des sauts conditionnels.

Les flags du processeur sont E (Equal), N (Non Equal), G (Greater), L (Lower)
Par soucis de simplicité, les flags représentent directement les résultats de la comparaison de deux valeurs entières signées.
On peux remarquer que N=G|L, ce qui rend ce flag inutile. Cependant il existe purement par but mnémotechnique : il est facile de se souvenir des flags avec le mot ENGLish.

```
|===============================================|  
|                  AX (16bits)                  |  
|=======================|=======================|  
|      AH (8bits)       |      AL (8bits)       |  
|=======================|=======================|  
```
```
|===============================================|  
|                  FR (8bits)                   |  
|=====|=====|=====|=====|=======================|  
|  E  |  N  |  G  |  L  |           /           |  
|=====|=====|=====|=====|=======================|  
```

Les instructions en elle-même sont représentées sur 4bits, les 12 autres étant les paramètres de l'instruction (voir [Syntaxe](#syntaxe-du-langage-dassemblage)) 

### RAM
Le bus d'adresse étant limité à 8bits, on peut en tout adresser 256 octets de mémoire.
Les adresses de 0xFF00 à 0xFFFF sont réservées pour la VRAM. Cette partie de la RAM est destinée à recevoir des valeurs de caractères du tableau ASCII. 

### Afficheur
L'affichage du contenu de la VRAM est effectué par des afficheurs 14 segments. Les caractères gérés par ces afficheurs sont limités aux valeurs parmis 0-9 et A-Z. Tout autre valeur sera affiché comme un espace vide. Cette limitation est due premièrement au fait qu'un ordinateur de démonstration comme celui-ci n'a pas besoin de plus de caractères pour afficher quelque chose de pertinent, et secondement car les afficheurs sont suffisament gros avec ces seuls caractères implémentés. 

## Description de l'assembleur
WIP

### Compilateur
WIP

### Syntaxe du langage d'assemblage
WIP

## Auteur
- [Louis Foulou](https://gitlab.com/Personne314)

## Installation
La map a été créée sous Minecraft Java 1.20.4 et n'a pas été testée avec d'autres versions.
Une fois compilé, le programme fournis permet de traduire un fichier source assembleur en code machine via :
```
./executable source destination
```

## Conditions d'utilisation
Vous êtes libres de réutiliser et de modifier ce projet comme bon vous semble pour peu que le nom de l'auteur soit cité. Merci de ne pas me voler mon travail.
