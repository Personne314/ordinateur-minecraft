#include "compiler.h"

#include <cctype>
#include <iostream>
#include <fstream>

#include "parser.h"
#include "analyser.h"



// Compile le programme.
// Renvoie une liste d'instructions sur 16bit.
bool programCompiler(string program, vector<unsigned short>& bin_code) {
    for (unsigned int i = 0; i < program.size(); i++) program[i] = std::toupper(program[i]);

    // Parse le programme et effectue son analyse sémantique.
    vector<Instruction*> instructions;
    if (!programParser(program, instructions)) {
        std::cout << "\033[1;31mfatal error:\033[0m unable to parse the program" << std::endl;
        goto COMPILER_ERROR;
    }
    if (!instructionAnalyser(instructions)) {
        std::cout << "\033[1;31mfatal error:\033[0m unable to do the semantical"
            " analysis of the program" << std::endl;
        goto COMPILER_ERROR;
    }

    // Parcourt les instructions et les traduit en langage machine.
    for (Instruction* instruction : instructions) {
        switch (instruction->type) {
        case REG_REG:
            bin_code.push_back(
                (((short)(((RegRegInstruction*)instruction)->instr) +
                (short)(((RegRegInstruction*)instruction)->reg1)) << 8) +
                (((short)(((RegRegInstruction*)instruction)->reg2)) << 4)
            );
            break;
        case REG:
            bin_code.push_back(
                ((short)(((RegInstruction*)instruction)->instr) +
                (short)(((RegInstruction*)instruction)->reg)) << 8
            );
            break;
        case REG_CONST:
            switch (((RegConstInstruction*)instruction)->instr) {
            case 0b10000000: case 0b10100000: case 0b10110000:
                bin_code.push_back(
                    (((short)(((RegConstInstruction*)instruction)->instr) +
                    (short)(((RegConstInstruction*)instruction)->reg)) << 8) +
                    ((short)(((RegConstInstruction*)instruction)->cte))
                );
                break;
            default:
                if (((RegConstInstruction*)instruction)->cte > 0b00111111) {
                    std::cout << "\033[1;31mfatal error:\033[0m ADD, SUB and CMP can't have"
                        " constant greater than 63" << std::endl;
                    goto COMPILER_ERROR;
                }
                bin_code.push_back(
                    (((short)(((RegConstInstruction*)instruction)->instr) +
                    (short)(((RegConstInstruction*)instruction)->reg)) << 8) +
                    ((short)(((RegConstInstruction*)instruction)->cte | 0b11000000))
                );
            }
            break;
        case CONST_CONST:
            bin_code.push_back(
                ((short)(((ConstConstInstruction*)instruction)->cte1) << 8) +
                ((short)(((ConstConstInstruction*)instruction)->cte2))
            );
            break;
        case CONST:
            bin_code.push_back(
                ((short)(((ConstInstruction*)instruction)->instr) << 8) +
                ((short)(((ConstInstruction*)instruction)->cte))
            );
            break;
        }
    }

    // Compilation réussie.
    for (Instruction* instruction : instructions) delete instruction;
    return true;

    // Gestion des cas d'erreur.
COMPILER_ERROR:
    for (Instruction* instruction : instructions) delete instruction;
    return false;

}



// Fonction d'affichage.
bool saveBinary(const string& path, const vector<unsigned short>& bin_code) {
    std::ofstream file;
    file.open(path);
    if (!file.is_open()) return false;

    // Parcourt chaque instruction.
    for (unsigned short num : bin_code) {

        // Parcourt chaque bit, de droite à gauche.
        for (int i = 15; i >= 0; --i) {
            file << ((num & (1 << i)) ? '1' : '0');
            if (i % 4 == 0) file << ' ';
        }
        file << std::endl;

    }
    file.close();
    return true;

}
