#pragma once

#include <string>
#include <vector>

using std::vector, std::string;



// Compile le programme.
bool programCompiler(string program, vector<unsigned short>& bin_code);

// Fonction d'affichage.
bool saveBinary(const string& path, const vector<unsigned short>& bin_code);
