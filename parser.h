#pragma once

#include <vector>
#include <string>
#include <unordered_map>

#include "lexer.h"

using std::string, std::unordered_map, std::vector;



// Type pour les instructions.
enum InstructionType {
    REG_REG,
    REG_CONST,
    CONST_CONST,
    REG,
    CONST
};

// Structure pour les instructions.
struct Instruction {
    InstructionType type;
    Instruction(InstructionType t) : type(t) {};
};

// Structure pour une opération entre deux registres.
struct RegRegInstruction : public Instruction {
    unsigned char instr;
    unsigned char reg1;
    unsigned char reg2;
    RegRegInstruction(unsigned char i, unsigned char r1, unsigned char r2) : Instruction(REG_REG), instr(i), reg1(r1), reg2(r2) {};
};

// Structure pour une opération sur un registre seul.
struct RegInstruction : public Instruction {
    unsigned char instr;
    unsigned char reg;
    RegInstruction(unsigned char i, unsigned char r) : Instruction(REG),  instr(i), reg(r) {};
};

// Structure pour une opération entre un registre et une valeur constante.
struct RegConstInstruction : public Instruction {
    unsigned char instr;
    unsigned char reg;
    unsigned char cte;
    RegConstInstruction(unsigned char i, unsigned char r, unsigned char c) : Instruction(REG_CONST),  instr(i), reg(r), cte(c) {};
};

// Structure pour FILL.
struct ConstConstInstruction : public Instruction {
    unsigned char cte1;
    unsigned char cte2;
    ConstConstInstruction(unsigned char c1, unsigned char c2) : Instruction(CONST_CONST), cte1(c1), cte2(c2) {};
};

// Structure pour JMP.
struct ConstInstruction : public Instruction {
    unsigned char instr;
    unsigned char cte;
    ConstInstruction(unsigned char i, unsigned char c) : Instruction(CONST), instr(i), cte(c) {};
};



// Fonction du parser.
bool programParser(const string& program, vector<Instruction*>& instructions);

// Evalue la valeur des labels.
bool evaluateLabels(const vector<Token*>& tokens, unordered_map<string, unsigned char>& labels);

// Renvoie la valeur d'un token.
// Hyp : token->type == LITTERAL_NUM, LITTERAL_CHAR, REGISTER, LABEL ou MNEMONIC.
unsigned short evaluateToken(Token* token, const unordered_map<string, unsigned char>& labels);

// Fonction d'affichage.
void printInstruction(Instruction* instruction);
