#include "analyser.h"

#include <iostream>



// Effectue l'analyse sémantique des instructions.
bool instructionAnalyser(vector<Instruction*>& instructions) {

    unsigned int instr = 1;
    for (Instruction* instruction : instructions) {
        switch (instruction->type) {
        case REG_REG:
            switch (((RegRegInstruction*)instruction)->instr) {
            case 0b00000000: case 0b00010000: case 0b00100000: case 0b00110000:
            case 0b01000000: case 0b01010000: case 0b01100000: case 0b01110000:
            case 0b11100000:
                if ((((RegRegInstruction*)instruction)->reg1 & 0x0C) == 0x0C ||
                    (((RegRegInstruction*)instruction)->reg2 & 0x0C) == 0x0C) {
                    std::cout << "\033[1;31manalyser error:\033[0m this mnemonic can't have"
                        "PC, IR, SR or FR as a parameter (instruction " << instr << ")" << std::endl;
                    return false;                    
                }
                break;
            default:
                std::cout << "\033[1;31manalyser error:\033[0m this mnemonic can't have"
                    "2 registers as parameters (instruction " << instr << ")" << std::endl;
                return false;
            }
            break;
        case REG:
            switch (((RegInstruction*)instruction)->instr) {
            case 0b11000000: case 0b11010000:
                break;
            default:
                std::cout << "\033[1;31manalyser error:\033[0m this mnemonic can't have"
                    "one register as parameter (instruction " << instr << ")" << std::endl;
                return false;
            }
            break;
        case REG_CONST:
            switch (((RegConstInstruction*)instruction)->instr) {
            case 0b00000000:
                ((RegConstInstruction*)instruction)->instr = 0b10000000;
                break;
            case 0b10100000: case 0b10110000: case 0b11100000:
            case 0b01000000: case 0b01010000:
                break;
            default:
                std::cout << "\033[1;31manalyser error:\033[0m this mnemonic can't have"
                    "one register and one constant as parameter (instruction " << instr << ")" << std::endl;
                return false;
            }
            break;
        default:
            break;
        }

        instr++;

    }
    return true;

}
