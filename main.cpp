#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#include "compiler.h"



// Main.
// Prend en argument un chemin vers le fichier assembleur et
// un second vers le fichier de sortie.
int main(int argc, char* argv[]) {
    std::cout << std::hex << std::uppercase;

    // Vérifie le nombre d'arguments.
    if (argc < 2) {
        std::cout << "\033[1;31mfatal error:\033[0m no input file" << std::endl;
        return EXIT_FAILURE;
    } else if (argc < 3) {
        std::cout << "\033[1;31mfatal error:\033[0m no output file" << std::endl;
        return EXIT_FAILURE;
    } else if (argc > 3) {
        std::cout << "\033[1;31mfatal error:\033[0m too much arguments" << std::endl;
        return EXIT_FAILURE;
    }

    // Lit le fichier source.
    std::ifstream file(argv[1]);
    if (!file.is_open()) {
        std::cout << "\033[1;31mfatal error:\033[0m couldn't open input file" << std::endl;
        return EXIT_FAILURE;
    }
    std::stringstream buffer;
    buffer << file.rdbuf();

    // Compile le programme.
    vector<unsigned short> bin_code;
    if (!programCompiler(buffer.str(), bin_code)) {
        std::cout << "\033[1;31mfatal error:\033[0m couldn't compile input file" << std::endl;
        return EXIT_FAILURE;
    }

    // Enregistre le resultat.
    if (!saveBinary(argv[2], bin_code)) {
        std::cout << "\033[1;31mfatal error:\033[0m couldn't open output file" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;

}
