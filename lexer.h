#pragma once

#include <string>
#include <vector>

using std::string, std::vector;



// Type pour les tokens et les lexèmes.
enum TokenType {
    MNEMONIC,
    LABEL,
    REGISTER,
    LITTERAL_CHAR,
    LITTERAL_NUM,
    SEPARATOR,
    START_INSTRUCTION
};

// Structure pour modéliser un lexème.
struct Lexeme {
    TokenType type;
    string value;
    Lexeme(TokenType t, string v) : type(t), value(v) {};
};



// Structure pour les tokens.
struct Token {
    TokenType type;
    Token(TokenType t) : type(t) {};
};

// Structure pour les caractères.
struct CharToken : public Token {
    char value;
    CharToken(char v) : Token(LITTERAL_CHAR), value(v) {};
};

// Structure pour les nombres.
struct NumToken : public Token {
    int value;
    NumToken(int v) : Token(LITTERAL_NUM), value(v) {};
};

// Structure pour les labels.
struct LabelToken : public Token {
    string value;
    LabelToken(string v) : Token(LABEL), value(v) {};
};

// Structure pour les séparateurs.
struct SeparatorToken : public Token {
    SeparatorToken() : Token(SEPARATOR) {};
};

// Structure pour les début d'instruction.
struct StartInstrToken : public Token {
    unsigned int line;
    StartInstrToken(unsigned int l) : Token(START_INSTRUCTION), line(l) {};
};

// Structure pour les registres.
struct RegisterToken : public Token {
    unsigned char value;
    RegisterToken(unsigned char v) : Token(REGISTER), value(v) {};
};

// Structure pour les mots clef.
struct MnemonicToken : public Token {
    unsigned char value;
    MnemonicToken(unsigned char v) : Token(MNEMONIC), value(v) {};
};



// Fonction du lexer.
bool programLexer(const string& program, vector<Token*>& tokens);



// Fonctions de scan.
Lexeme* scanWord(string word);
Lexeme* scanRegister(string word);
Lexeme* scanLitteralChar(string word);
Lexeme* scanLitteralNum(string word);
Lexeme* scanMnemonic(string word);
Lexeme* scanSeparator(string word);
Lexeme* scanLabel(string word);

// Fonctions d'affichage.
void printLexeme(Lexeme* lexeme);
void printToken(Token* token);
