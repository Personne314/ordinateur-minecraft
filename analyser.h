#pragma once

#include <vector>

#include "parser.h"

using std::vector;



// Effectue l'analyse sémantique des instructions.
bool instructionAnalyser(vector<Instruction*>& instructions);
