#include "parser.h"

#include <iostream>
#include <iomanip>



// Permet de construire l'arbre de syntaxe du programme.
bool programParser(const string& program, vector<Instruction*>& instructions) {
    unsigned int start = 0;
    unsigned int end = 0;

    // Récupère les tokens et les valeurs des labels.
    vector<Token*> tokens;
    unordered_map<string, unsigned char> labels;
    if (!programLexer(program, tokens) || !evaluateLabels(tokens, labels)) {
        std::cout << "\033[1;31mfatal error:\033[0m unable to do the lexical"
            " analysis of the program" << std::endl;
        goto PARSE_ERROR; 
    }

    std::cout << "\033[1;32m||=============||\033[0m" << std::endl;
    std::cout << "\033[1;32m||    PARSER   ||\033[0m" << std::endl;
    std::cout << "\033[1;32m||=============||\033[0m" << std::endl;

    // Parcourt les tokens.
    while (start < tokens.size()) {
        unsigned int line = ((StartInstrToken*)tokens[start])->line;
        start++;

        // Avance end jusqu'à la fin de l'instruction qui commence en start.
        end = start;
        while (end < tokens.size() && tokens[end]->type != START_INSTRUCTION) end++;

        // Saute le label de début et les instructions vides.
        switch (tokens[start]->type) {
        case START_INSTRUCTION:
            continue;
        case LABEL:
            start++;
        default:
            break;
        }

        // Vérifie que le premier token (prmeier label exclu) est un mnémonique.
        if (tokens[start]->type != MNEMONIC) {
            std::cout << "\033[1;31mparser error:\033[0m label or mnemonic expected at the"
                "beginning of an instruction (line " << line << ")" << std::endl;
            goto PARSE_ERROR; 
        }

        // Parse l'instruction en fonction de sa longueur.
        switch (end-start) {

        // Instruction de forme INSTR VAL.
        case 2:
            switch (tokens[start+1]->type) {
            case REGISTER:
                instructions.push_back(new RegInstruction(
                    0x00FF & evaluateToken(tokens[start], labels),
                    0x00FF & evaluateToken(tokens[start+1], labels)
                ));
                break;
            case LITTERAL_CHAR: case LITTERAL_NUM: case LABEL:
                switch (((MnemonicToken*)tokens[start])->value) {
                case 0b10010000:
                    instructions.push_back(new ConstConstInstruction(
                        (0xFF00 & evaluateToken(tokens[start+1], labels)) >> 8,
                        0x00FF & evaluateToken(tokens[start+1], labels)
                    ));
                    break;
                case 0b11111100: case 0b11111000: case 0b11110100: case 0b11110010:
                case 0b11111010: case 0b11110001: case 0b11111001:
                    instructions.push_back(new ConstInstruction(
                        0x00FF & evaluateToken(tokens[start], labels),
                        0x00FF & evaluateToken(tokens[start+1], labels)
                    ));
                    break;
                default:
                    std::cout << "\033[1;31mparser error:\033[0m can't have a constant"
                        "parameter (line " << line << ")" << std::endl;
                    goto PARSE_ERROR;
                }
                break;
            default:
                std::cout << "\033[1;31mparser error:\033[0m separator, mnemonic or none are"
                    "not expected as arguments (line " << line << ")" << std::endl;
                goto PARSE_ERROR;
            }
            break;
           
        // Instruction de forme INSTR VAL1, VAL2.
        case 4:

            // Vérifie la présence d'un séparateur.
            if (tokens[start+2]->type != SEPARATOR) {
                std::cout << "\033[1;31mparser error:\033[0m separator expected (line " 
                    << line << ")" << std::endl;
                goto PARSE_ERROR;
            }

            // Match les opérandes.
            switch (tokens[start+1]->type) {
            case REGISTER:
                switch(tokens[start+3]->type) {
                case REGISTER:
                    instructions.push_back(new RegRegInstruction(
                        0x00FF & evaluateToken(tokens[start], labels),
                        0x00FF & evaluateToken(tokens[start+1], labels),
                        0x00FF & evaluateToken(tokens[start+3], labels)
                    ));
                    break;
                case LABEL: case LITTERAL_CHAR: case LITTERAL_NUM: 
                    instructions.push_back(new RegConstInstruction(
                        0x00FF & evaluateToken(tokens[start], labels),
                        0x00FF & evaluateToken(tokens[start+1], labels),
                        0x00FF & evaluateToken(tokens[start+3], labels)
                    ));                        
                    break;
                default:
                    std::cout << "\033[1;31mparser error:\033[0m const value or register"
                        "expected as second argument (line " << line << ")" << std::endl;
                    goto PARSE_ERROR;
                }                        
                break;
            case LABEL: case LITTERAL_CHAR: case LITTERAL_NUM:
                switch(tokens[start+3]->type) {
                case LABEL: case LITTERAL_CHAR: case LITTERAL_NUM:
                    if (((MnemonicToken*)tokens[start])->value == 0b10010000) {
                        instructions.push_back(new ConstConstInstruction(
                            0x00FF & evaluateToken(tokens[start+1], labels),
                            0x00FF & evaluateToken(tokens[start+3], labels)
                        ));
                    } else {
                        std::cout << "\033[1;31mparser error:\033[0m can't have 2 constant"
                            "parameters (line " << line << ")" << std::endl;
                        goto PARSE_ERROR;
                    }
                    break;
                default:
                    std::cout << "\033[1;31mparser error:\033[0m const value expected as second"
                        "argument (line " << line << ")" << std::endl;
                    goto PARSE_ERROR;
                }
                break;
            default:
                std::cout << "\033[1;31mparser error:\033[0m label or register expected"
                    "as first argument (line " << line << ")" << std::endl;
                goto PARSE_ERROR;
            }
            break;
        
        // On a pas le bon nombre de tokens pour construire une instruction.
        default:
            std::cout << "\033[1;31mparser error:\033[0m an instruction couldn't"
                "be parsed (line " << line << ")" << std::endl;
            goto PARSE_ERROR;        
        }

        // Passe à l'instruction suivante.
        start = end;

    }

    std::cout << "\033[1;32mInstructions :\033[0m" << std::endl;
    for (Instruction* instruction : instructions) printInstruction(instruction);

    // Analyse syntaxique termninée.
    for (Token* token : tokens) delete token;
    return true;

    // Gestion des cas d'erreur.
PARSE_ERROR:
    for (Token* token : tokens) delete token;
    return false; 

}



// Repère les tokens en début de ligne, vérifie leur unicité et leur attribue leur valeur.
bool evaluateLabels(const vector<Token*>& tokens, unordered_map<string, unsigned char>& labels) {
    
    // Parcourt les tokens.
    unsigned char pc = 0;
    unsigned int line = 1;
    unsigned int start = 0;
    while (start < tokens.size()-1) { 

        // Si on trouve un début d'instruction, recherche un label.
        // Si l'instruction n'est pas vide, avance pc.
        if (tokens[start]->type == START_INSTRUCTION) {
            if (tokens[start+1]->type == LABEL) {
                
                // Associe le label à sa valeur.
                LabelToken* token = (LabelToken*)tokens[start+1];
                if (labels.find(token->value) == labels.end()) {
                    labels[token->value] = pc;
                } else {
                    std::cout << "\033[1;31mparser error:\033[0m label \'" << token->value
                        << "\' is already defined (line " << line << ")" << std::endl;
                    return false;
                }   

            }
            
            // Incrémente pc de 2 si l'instruction n'est pas vide.
            if (tokens[start+1]->type != START_INSTRUCTION) pc += 2;
            line++;

        }
        start++;

    }

    // Vérifie que tout les labels utilisés sont bien définis.
    line = 0;
    start = 0;
    while (start < tokens.size()-1) { 
        if (tokens[start]->type == START_INSTRUCTION) line++;
        if (tokens[start]->type == LABEL) {
            if (labels.find(((LabelToken*)tokens[start])->value) == labels.end()) {
                std::cout << "\033[1;31mparser error:\033[0m label \'" << ((LabelToken*)tokens[start])->value
                    << "\' is used bunt not defined (line " << line << ")" << std::endl;
                return false;
            }
        }
        start++;
    }

    return true;

}



// Renvoie la valeur d'un token sur 16bit.
// Hyp : token->type == LITTERAL_NUM, LITTERAL_CHAR, REGISTER ou MNEMONIC.
unsigned short evaluateToken(Token* token, const unordered_map<string, unsigned char>& labels) {
    switch (token->type) {
    case LITTERAL_NUM:
        return 0x0000FFFF & ((NumToken*)token)->value;
    case LITTERAL_CHAR:
        return ((CharToken*)token)->value;
    case REGISTER:
        return ((RegisterToken*)token)->value;
    case MNEMONIC:
        return ((MnemonicToken*)token)->value;
    case LABEL:
        return labels.at(((LabelToken*)token)->value);
    default:
        return 0;
    }
}



// Fonction d'affichage.
void printInstruction(Instruction* instruction) {
    switch (instruction->type) {
    case REG_REG:
        std::cout << "REG-REG INSTR     : " << (unsigned int)((RegRegInstruction*)instruction)->instr << ", "
            << (unsigned int)((RegRegInstruction*)instruction)->reg1 << ", "
            << (unsigned int)((RegRegInstruction*)instruction)->reg2 << std::endl;
        break;
    case REG:
        std::cout << "REG INSTR         : " << (unsigned int)((RegInstruction*)instruction)->instr << ", "
            << (unsigned int)((RegInstruction*)instruction)->reg << std::endl;
        break;
    case REG_CONST:
        std::cout << "REG-CONST INSTR   : " << (unsigned int)((RegConstInstruction*)instruction)->instr << ", "
            << (unsigned int)((RegConstInstruction*)instruction)->reg << ", "
            << (unsigned int)((RegConstInstruction*)instruction)->cte << std::endl;
        break;
    case CONST:
        std::cout << "CONST INSTR       : " << (unsigned int)((ConstInstruction*)instruction)->instr << ", "
            << (unsigned int)((ConstInstruction*)instruction)->cte << std::endl;
        break;
    case CONST_CONST:
        std::cout << "CONST-CONST INSTR : " << (unsigned int)((ConstConstInstruction*)instruction)->cte1 << ", "
            << (unsigned int)((ConstConstInstruction*)instruction)->cte2 << std::endl;
        break;
    }
}
