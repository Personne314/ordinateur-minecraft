#include "lexer.h"

#include <iostream>
#include <cctype>
#include <unordered_map>
#include <sstream>

using std::istringstream, std::unordered_map;



// Map pour les mot clef.
const unordered_map<string, unsigned char> mnemonic_values = {
    {"MOV", 0b00000000},
    {"AND", 0b00010000},
    {"STR", 0b00100000},
    {"LDR", 0b00110000},
    {"ADD", 0b01000000},
    {"SUB", 0b01010000},
    {"OR",  0b01100000},
    {"XOR", 0b01110000},
    {"FILL",0b10010000},
    {"LSL", 0b10100000},
    {"LSR", 0b10110000},
    {"PUSH",0b11000000},
    {"POP", 0b11010000},
    {"CMP", 0b11100000},
    {"JMP", 0b11111100},
    {"JEQ", 0b11111000},
    {"JNE", 0b11110100},
    {"JGT", 0b11110010},
    {"JGE", 0b11111010},
    {"JLT", 0b11110001},
    {"JLE", 0b11111001}
};

// Map pour les registres.
const unordered_map<string, unsigned char> register_values = {
    {"AL", 0b00000000},
    {"BL", 0b00000001},
    {"CL", 0b00000010},
    {"DL", 0b00000011},
    {"AH", 0b00000100},
    {"BH", 0b00000101},
    {"CH", 0b00000110},
    {"DH", 0b00000111},
    {"AX", 0b00001000},
    {"BX", 0b00001001},
    {"CX", 0b00001010},
    {"DX", 0b00001011},
    {"PC", 0b00001100},
    {"IR", 0b00001101},
    {"SR", 0b00001110},
    {"FR", 0b00001111}
};


/*

e -> test == 0 en sortie du SUB
n -> test != 0 en sortie du SUB
g -> test == 0 sur bit de signe en sortie du SUB
l -> test == 1 sur bit de signe en sortie du SUB

FR -> engl0000

*/



// Permet d'effectuer l'analyse lexicale du programme.
bool programLexer(const string& program, vector<Token*>& tokens) {
    istringstream program_stream(program);

    std::cout << "\033[1;32m||=============||\033[0m" << std::endl;
    std::cout << "\033[1;32m||    LEXER    ||\033[0m" << std::endl;
    std::cout << "\033[1;32m||=============||\033[0m" << std::endl;
    std::cout << "\033[1;32mLexemes :\033[0m" << std::endl;

    string line;
    int line_num = 1;
    while (std::getline(program_stream, line)) {
        tokens.push_back(new StartInstrToken(line_num));

        unsigned int start = 0; 
        unsigned int end = 0;
        while (end < line.size()) {

            while (start < line.size() && std::isspace(line[start])) start++;

            end = start;
            while (end < line.size() && !(std::isspace(line[end]) || line[end] == ',')) end++;

            if (start != end) {

                Lexeme* lexeme = scanWord(line.substr(start, end-start));
                if (lexeme == nullptr) {
                    std::cout << "\033[1;31mlexer error:\033[0m " << line.substr(start, end-start)
                        << " isn't regognized (line " << line_num << ")" << std::endl;
                    return false;
                }
                printLexeme(lexeme);

                Token* token;
                switch (lexeme->type) {
                case MNEMONIC:
                    token = new MnemonicToken(mnemonic_values.at(lexeme->value));
                    break;
                case LABEL:
                    token = new LabelToken(lexeme->value);
                    break;
                case REGISTER:
                    token = new RegisterToken(register_values.at(lexeme->value));
                    break;
                case LITTERAL_CHAR:
                    token = new CharToken(lexeme->value[0]);
                    break;
                case LITTERAL_NUM:
                    if (lexeme->value.size() >= 2 && !std::isdigit(lexeme->value[1])) {
                        if (lexeme->value[1] == 'X') {
                            token = new NumToken(std::stoul(lexeme->value.substr(2, lexeme->value.size()-2), nullptr, 16));
                        } else { 
                            token = new NumToken(std::stoul(lexeme->value.substr(2, lexeme->value.size()-2), nullptr, 2));
                        }
                    } else {
                        token = new NumToken(std::stoul(lexeme->value, nullptr, 10));
                    }
                    break;
                default:
                    break;
                }

                tokens.push_back(token);
                delete lexeme;
                start = end;

            }
            
            if (line[end] == ',') {
                tokens.push_back(new SeparatorToken());
                start++;
            }
        }

        line_num++;

    }

    std::cout << "\033[1;32mTokens :\033[0m" << std::endl;
    for (Token* token : tokens) printToken(token);
    return true;

}



// Permet de scanner un mot via les fonctions de scan suivantes.
Lexeme* scanWord(string word) {

    // Scan le mot.
    Lexeme* lexeme = scanRegister(word);
    if (lexeme == nullptr) lexeme = scanMnemonic(word);
    if (lexeme == nullptr) lexeme = scanLitteralChar(word);
    if (lexeme == nullptr) lexeme = scanLitteralNum(word);
    if (lexeme == nullptr) lexeme = scanLabel(word);
    return lexeme;

}

// Permet de scanner un registre parmis :
// AX, AL, BX, BL, CX, CL, DX, DL, PC, IR, SR, FR. 
Lexeme* scanRegister(string word) {
    if (word.size() != 2) return nullptr;
    
    // Parcourt le mot.
    unsigned int i = 0;
    unsigned int state = 0;
    while (i < word.size()) {

        // Effectue une étape de l'exécution de l'automate.
        switch (state) {
        case 0:
            switch (word[i]) {
            case 'A': case 'B': case 'C': case 'D':
                state = 1;
                break;
            case 'I': case 'S': case 'F':
                state = 2;
                break;
            case 'P':
                state = 3;
                break;
            default:
                return nullptr;
            }
            break;
        case 1:
            switch (word[i]) {
            case 'L': case 'X': case 'H':
                return new Lexeme(REGISTER, word);
            default:
                return nullptr;
            }
            break;
        case 2:
            switch (word[i]) {
            case 'R':
                return new Lexeme(REGISTER, word);
            default:
                return nullptr;
            }
        case 3:
            switch (word[i]) {
            case 'C':
                return new Lexeme(REGISTER, word);
            default:
                return nullptr;
            }
        }
        i++;

    }
    return nullptr;

}

// Permet de scanner un caractère '.' ou '\0'.
Lexeme* scanLitteralChar(string word) {
    if (word.size() > 4 || word.size() < 3) return nullptr;
    
    unsigned int i = 0;
    unsigned int state = 0;
    while (i < word.size()) {

        // Effectue une étape de l'exécution de l'automate.
        switch (state) {
        case 0:
            if (word[i] != '\'') return nullptr;
            state = 1;
            break;              
        case 1:
            if (word[i] == '\\') state = 2;
            else if (std::isalnum(word[i]) || word[i] == ' ') state = 3;
            else return nullptr;
            break;
        case 2:
            if (word[i] != '0') return nullptr;
            state = 3;
            break;        
        case 3:
            if (word[i] == '\'') {
                string str; 
                str += std::toupper(word[1]);
                if (word.size() == 4) str = "\0";
                return new Lexeme(LITTERAL_CHAR, str);
            }
            return nullptr;
        }
        i++;

    }
    return nullptr;

}

// Permet de scanner un nombre en base 2, 10 ou 16.
Lexeme* scanLitteralNum(string word) {
    switch (word[0]) {
    case '0': case '1': case '2': case '3': case '4': case '5': 
    case '6': case '7': case '8': case '9': case '-': case '+': 
        break;
    default:
        return nullptr;
    }

    unsigned int i = 0;
    unsigned int state = 0;
    while (i < word.size()) {
    
        // Effectue une étape de l'exécution de l'automate.
        switch (state) {
        case 0:
            if (word[0] == '0') state = 1;
            else state = 2;
            break;
        case 1:
            switch (word[i]) {
            case 'X':
                state = 3;
                break;
            case 'B':
                state = 4;
                break;
            default:
                if (!std::isdigit(word[i])) return nullptr;
                state = 2;
            }
            break;
        case 2:
            if (!std::isdigit(word[i])) return nullptr; 
            break;
        case 3:
            if (!std::isxdigit(word[i])) return nullptr;
            break; 
        case 4:
            if (word[i] < '0' || word[i] > '1') return nullptr; 
        }
        i++;

    }

    // Si on arrive ici, word à le bon format pour un nombre.
    return new Lexeme(LITTERAL_NUM, word);

}

// Permet de scanner un mot clef du langage parmis :
// ADD, SUB, LDR, STR, MOV, AND, OR, XOR, LSL, LSR, PUSH, POP, CMP, JMP, JEQ, JNE, JGT, JGE, JLT, JLE.
Lexeme* scanMnemonic(string word) {

    unsigned int i = 0;
    unsigned int state = 0;
    while (i < word.size()) {

        // Effectue une étape de l'exécution de l'automate.
        // Celui là il est autant horrible à lire qu'il a été à écrire.
        switch (state) {
        case 0:
            switch (word[i]) {
            case 'A':
                state = 1;
                break;
            case 'S':
                state = 2;
                break;
            case 'L':
                state = 3;
                break;
            case 'M':
                state = 4;
                break;
            case 'O':
                state = 5;
                break;
            case 'X':
                state = 6;
                break;
            case 'P':
                state = 7;
                break;
            case 'C':
                state = 8;
                break;
            case 'J':
                state = 9;
                break;
            case 'F':
                state = 21;
                break;
            default:
                return nullptr;
            } 
            break;
        case 1:
            switch (word[i]) {
            case 'N': case 'D':
                state = 10;
                break;
            default:
                return nullptr;
            }
            break;
        case 2:
            switch (word[i]) {
            case 'U':
                state = 11;
                break;
            case 'T':
                state = 5;
                break;
            default:
                return nullptr;
            }            
            break;
        case 3:
            switch (word[i]) {
            case 'S':
                state = 12;
                break;
            case 'D':
                state = 5;
                break;
            default:
                return nullptr;
            }   
            break;
        case 4:
            if (word[i] != 'O') return nullptr;
            state = 13;
            break;
        case 5:
            if (word[i] != 'R') return nullptr;
            state = 20;
            break;
        case 6: 
            if (word[i] != 'O') return nullptr;
            state = 5;
            break;
        case 7:
            switch (word[i]) {
            case 'U':
                state = 14;
                break;
            case 'O':
                state = 16;
                break;
            default:
                return nullptr;
            } 
            break;
        case 8:
            if (word[i] != 'M') return nullptr;
            state = 16;
            break;
        case 9:
            switch (word[i]) {
            case 'M':
                state = 16;
                break;
            case 'E':
                state = 17;
                break;
            case 'N':
                state = 18;
                break;
            case 'G': case 'L':
                state = 19;
                break;
            default:
                return nullptr;
            }  
            break;
        case 10:
            if (word[i] != 'D') return nullptr;
            state = 20;
            break;
        case 11:
            if (word[i] != 'B') return nullptr;
            state = 20;
            break;
        case 12:
            switch (word[i]) {
            case 'L': case 'R':
                state = 20;
                break;
            default:
                return nullptr;
            }  
            break;
        case 13:
            if (word[i] != 'V') return nullptr;
            state = 20;
            break;
        case 14:
            if (word[i] != 'S') return nullptr;
            state = 15;
            break;
        case 15:
            if (word[i] != 'H') return nullptr;
            state = 20;
            break;
        case 16:
            if (word[i] != 'P') return nullptr;
            state = 20;
            break;
        case 17:
            if (word[i] != 'Q') return nullptr;
            state = 20;
            break;
        case 18:
            if (word[i] != 'E') return nullptr;
            state = 20;
            break;
        case 19:
            switch (word[i]) {
            case 'T': case 'E':
                state = 20;
                break;
            default:
                return nullptr;
            }
            break;
        case 20:
            return nullptr;
            break;
        case 21:
            if (word[i] != 'I') return nullptr;
            state = 22;
            break;
        case 22:
            if (word[i] != 'L') return nullptr;
            state = 23;
            break;
        case 23:
            if (word[i] != 'L') return nullptr;
            state = 20;
            break;
        }

        i++;

    }

    // Si on arrive dans l'état 20, alors on a touvé un mot clef.
    if (state != 20) return nullptr;
    return new Lexeme(MNEMONIC, word);

}

// Permet de scanner les séparateurs.
Lexeme* scanSeparator(string word) {
    if (word == ",") return new Lexeme(SEPARATOR, ",");
    return nullptr;
}

// Permet de scanner un label.
// Ne vérifie pas que le label n'est pas un registre ou un mot clef.
Lexeme* scanLabel(string word) {

    // Parcourt le mot.
    unsigned int i = 0;
    unsigned int state = 0;
    while (i < word.size()) {

        // Effectue une étape de l'exécution de l'automate.
        switch (state) {
        case 0:
            if (!std::isalpha(word[i]) && word[i] != '_') return nullptr;
            state = 1;
            break;
        case 1:
            if (!std::isalnum(word[i]) && word[i] != '_') return nullptr;
        }

        i++;

    }

    // Si on arrive ici, le mot est au bon format pour un label.
    return new Lexeme(LABEL, word);

}



// Fonction d'affichage.
void printLexeme(Lexeme* lexeme) {
    switch (lexeme->type) {
    case MNEMONIC:
        std::cout << "MNEMONIC LEXEME      : " << lexeme->value << std::endl;
        break; 
    case LABEL:
        std::cout << "LABEL LEXEME         : " << lexeme->value << std::endl;
        break;
    case REGISTER:
        std::cout << "REGISTER LEXEME      : " << lexeme->value << std::endl;
        break;
    case LITTERAL_CHAR:
        std::cout << "LITTERAL CHAR LEXEME : " << lexeme->value << std::endl;
        break;
    case LITTERAL_NUM:
        std::cout << "LITTERAL NUM LEXEME  : " << lexeme->value << std::endl;
        break;
    default:
        break;
    }
}

// Fonction d'affichage.
void printToken(Token* token) {
    switch (token->type) {
    case MNEMONIC:
        std::cout << "MNEMONIC TOKEN      : " << (unsigned short)(((MnemonicToken*)token)->value) << std::endl;
        break; 
    case LABEL:
        std::cout << "LABEL TOKEN         : " << ((LabelToken*)token)->value << std::endl;
        break;
    case REGISTER:
        std::cout << "REGISTER TOKEN      : " << (unsigned short)(((RegisterToken*)token)->value) << std::endl;
        break;
    case LITTERAL_CHAR:
        std::cout << "LITTERAL CHAR TOKEN : \'" << ((CharToken*)token)->value << "\'" << std::endl;
        break;
    case LITTERAL_NUM:
        std::cout << "LITTERAL NUM TOKEN  : " << ((NumToken*)token)->value << std::endl;
        break;
    case SEPARATOR:
        std::cout << "SEPARATOR TOKEN     : ," << std::endl;
        break;
    case START_INSTRUCTION:
        std::cout << "START INSTRUCTION TOKEN" << std::endl;
        break;
    }
}
