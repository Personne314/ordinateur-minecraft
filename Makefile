compiler: compiler.o analyser.o lexer.o parser.o main.cpp
	g++ $^ -o $@ -Wall

%.o: %.cpp %.h
	g++ -c $< -o $@ -Wall
